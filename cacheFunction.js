function cacheFunction(cb) {
    let cache = {}
    let counter = 0
    return function(...rest) {
        let args = ""
        for(i = 0 ; i < rest.length ; i++) {
            args += rest[i].toString()
        }
        if (args in cache) {
            return cache[args] ;
        }
        let result  = cb(...rest) ;
        cache[args] = result ;
        return result
    }
}

module.exports = cacheFunction ;