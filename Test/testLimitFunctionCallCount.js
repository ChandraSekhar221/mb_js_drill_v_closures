const limitFunctionCallCount = require('../limitFunctionCallCount')
const cb = (a,b) => a+b ;
const result = limitFunctionCallCount(cb,2)
console.log(result(2,3))
console.log(result(3,4))
console.log(result(4,5))
console.log(result(5,6))
