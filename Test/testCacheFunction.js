const cacheFunction = require('../cacheFunction')
const add = (a,b,c) => a+b+c ;
const product = (a,b,c,d) => a*b*c*d;
const div = (x,y) => x/y ;
const addResult = cacheFunction(add)
console.log(addResult(1,2,3))
console.log(addResult(1,2,3))
console.log(addResult(3,4,5))
const proResult = cacheFunction(product)
console.log(proResult(1,2,3,4))
console.log(proResult(1,2,3,4))
console.log(proResult(1,4,3,2))
const divResult = cacheFunction(div)
console.log(divResult(6,3))
console.log(divResult(6,3))
console.log(divResult(9,3))

