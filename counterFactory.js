const counterFactory = () =>{
    let counter = 0 
    return (function() { let myObj = {
        increment : () => { return counter = counter + 1},
        decrement : () => { return counter = counter - 1}
        }  
        return myObj                 
    })()
} ;

module.exports = counterFactory ;
