function limitFunctionCallCount(cb,n) {
    let counter = 0 
    return function(x,y) {
        counter  += 1
        return counter <= n ? cb(x,y) :  null
    }
}

module.exports = limitFunctionCallCount ;